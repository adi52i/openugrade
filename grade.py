TASKS = [
    (100, 12),
    (100, 12),
    (100, 12),
]
MINIMUM_POINTS = 36

def calculate_final_grade(exam_grade):
    accumulated_points = sum([p[1] for p in TASKS])
    if accumulated_points < MINIMUM_POINTS:
        raise Exception('Not enough points, {} < {}'.format(accumulated_points, MINIMUM_POINTS))

    TASKS.sort(key=lambda task: task[0], reverse=True)

    final_grade = 0
    tasks_weight = 0
    for index, task in enumerate(TASKS):
        if tasks_weight < MINIMUM_POINTS or task[0] > exam_grade:
            final_grade += 1.0 * task[0] * task[1]
            tasks_weight += task[1]

    exam_weight = 100 - tasks_weight
    final_grade += exam_weight * exam_grade * 1.0

    print('{}\t{}'.format(exam_grade, int(round(final_grade / 100))))


print('Tasks: ' + str(TASKS))
print()
print('{}\t{}'.format('EXAM', 'FINAL'))
calculate_final_grade(100)
calculate_final_grade(95)
calculate_final_grade(90)
calculate_final_grade(85)
calculate_final_grade(80)
calculate_final_grade(75)
calculate_final_grade(70)
calculate_final_grade(60)

